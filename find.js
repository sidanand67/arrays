function find(elements, cb){
    let result; 
    let arrayLen = elements.length; 
    for(let i = 0; i < arrayLen; i++){
        if (Boolean(cb(elements[i], i, elements)) === true){
            result = elements[i]; 
            break;
        } 
    }
    return result; 
}

module.exports = find