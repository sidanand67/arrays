const reduce = require('./reduce'); 

function flatten(elements, depth=1){
    let result = elements, prev; 
    let index = 0; 
    while(index < depth && JSON.stringify(result) !== JSON.stringify(prev)){
        prev = result; 
        result = reduce(result, (final,current) => {
            if (current !== undefined){
                return final.concat(current)
            }
            return final; 
        }, []);      
        index++; 
    }
    return result;
}

module.exports = flatten; 