function reduce(elements, cb, startingValue){
    let result; 
    if(startingValue === undefined){
        result = elements[0]; 
    } 
    else {
        result = startingValue; 
    }
    for(let index = 0;index < elements.length; index++){
        if (index === 0 && startingValue === undefined){
            continue; 
        }
        result = cb(result, elements[index], index, elements); 
    }

    return result; 
}

module.exports = reduce; 