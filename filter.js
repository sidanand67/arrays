function filter(elements, cb){
    let result = []; 
    let arrayLen = elements.length; 
    for(let index = 0; index < arrayLen; index++){
        if(cb(elements[index], index, elements) === true){
            result.push(elements[index]); 
        } 
    }
    return result;
}

module.exports = filter; 
