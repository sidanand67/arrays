function each(elements, cb){
    let arrayLen = elements.length; 
    for(let index = 0; index < arrayLen; index++){
        cb(elements[index], index, elements); 
    }
}

module.exports = each 